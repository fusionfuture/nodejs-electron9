#!/usr/bin/env bash
_builddir=$1

export CC=clang
export CXX=clang++
export AR=ar
export NM=nm

# Workaround for building with ICU 68.x
CPPFLAGS+=' -DU_DEFINE_FALSE_AND_TRUE=1'

CFLAGS="${CFLAGS/-fexceptions/}"
CXXFLAGS="${CXXFLAGS/-fexceptions/}"

# This appears to cause random segfaults when combined with ThinLTO
# https://bugs.archlinux.org/task/73518
CFLAGS=${CFLAGS/-fstack-clash-protection}
CXXFLAGS=${CXXFLAGS/-fstack-clash-protection}

# Do not warn about unknown warning options
CFLAGS+='   -Wno-unknown-warning-option'
CXXFLAGS+=' -Wno-unknown-warning-option'

cd ${_builddir}/src
export CHROMIUM_BUILDTOOLS_PATH="${PWD}/buildtools"
GN_EXTRA_ARGS='
blink_symbol_level = 0
clang_use_chrome_plugins = false
custom_toolchain = "//build/toolchain/linux/unbundle:default"
host_toolchain = "//build/toolchain/linux/unbundle:default"
icu_use_data_file = false
is_component_ffmpeg = false
link_pulseaudio = true
linux_use_bundled_binutils = false
treat_warnings_as_errors = false
use_custom_libcxx = false
use_gnome_keyring = false
use_sysroot = false
'
chmod 0755 ${_builddir}/gn-m85
${_builddir}/gn-m85 gen out/Release \
    --args="import(\"//electron/build/args/release.gn\") ${GN_EXTRA_ARGS}"
ninja -C out/Release electron
# Strip before zip to avoid
# zipfile.LargeZipFile: Filesize would require ZIP64 extensions
strip -s out/Release/electron
ninja -C out/Release electron_dist_zip
# ninja -C out/Release third_party/electron_node:headers
