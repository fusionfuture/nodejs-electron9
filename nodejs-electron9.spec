%global debug_package %{nil}
%define mod_name electron9
%define program_version 9.4.4

Name:           nodejs-%{mod_name}
Version:        %{program_version}
Release:        0
Summary:        Build cross platform desktop apps with JavaScript, HTML, and CSS
License:        MIT
Group:          Productivity/Networking/Web/Browsers
URL:            https://github.com/electron/electron
Source0:        https://gitlab.com/fusionfuture/nodejs-electron9/-/raw/main/gn-m85
Source1:        https://gitlab.com/fusionfuture/nodejs-electron9/-/raw/main/prep.sh
Source2:        https://gitlab.com/fusionfuture/nodejs-electron9/-/raw/main/build.sh

BuildRequires:  /usr/bin/python2
BuildRequires:  /usr/bin/python3
BuildRequires:  /usr/bin/npm
BuildRequires:  /usr/bin/yarn
BuildRequires:  /usr/bin/clang
BuildRequires:  /usr/bin/clang++
BuildRequires:  bsdtar
BuildRequires:  git
BuildRequires:  gperf
BuildRequires:  harfbuzz-devel
BuildRequires:  hicolor-icon-theme
BuildRequires:  java-11-openjdk-headless
BuildRequires:  jsoncpp-devel
BuildRequires:  libnotify-devel
BuildRequires:  llvm
BuildRequires:  ninja
BuildRequires:  pciutils-devel
BuildRequires:  python38-httplib2
BuildRequires:  python38-pyparsing
BuildRequires:  python38-six
BuildRequires:  wget
BuildRequires:  yasm-devel
ExclusiveArch:  x86_64

Provides:       %{mod_name}
Obsoletes:      nodejs-%{mod_name}-prebuilt <= %{version}

%description
Nodejs application: Build cross platform desktop apps with JavaScript, HTML, and CSS

%prep
cp %{SOURCE0} %{_builddir}/gn-m85

%build
bash %{SOURCE1} %{_builddir}
bash %{SOURCE2} %{_builddir}

%install
install -dm755 %{buildroot}%{_libdir}/%{mod_name}
bsdtar -xf %{_builddir}/src/out/Release/dist.zip -C "%{buildroot}%{_libdir}/%{mod_name}"

chmod u+s "%{buildroot}%{_libdir}/%{mod_name}/chrome-sandbox"

install -dm755 %{buildroot}%{_bindir}
ln -s ../..%{_libdir}/%{mod_name}/electron %{buildroot}%{_bindir}/%{mod_name}

# Install .desktop and icon file (see default_app-icon.patch)
install -Dm644 -t %{buildroot}%{_datadir}/applications %{_builddir}/patches/%{mod_name}.desktop
install -Dm644 %{_builddir}/src/electron/default_app/icon.png %{buildroot}%{_datadir}/pixmaps/%{mod_name}.png

%files
%license %{_libdir}/%{mod_name}/LICENSE
%{_bindir}/%{mod_name}
%{_datadir}/pixmaps/%{mod_name}.png
%{_datadir}/applications/%{mod_name}.desktop
%{_libdir}/%{mod_name}/

%changelog

