#!/usr/bin/env bash
mod_name=electron9
version=9.4.4
_chromiumver=83.0.4103.122
_builddir=$1

# Check dependencies
which git >/dev/null 2>&1 || exit 1
which python2 >/dev/null 2>&1 || exit 1
which python3 >/dev/null 2>&1 || exit 1
which yarn >/dev/null 2>&1 || exit 1
which ninja >/dev/null 2>&1 || exit 1

cd ${_builddir}

mkdir -p ${_builddir}/python2-path
ln -sf /usr/bin/python2 "${_builddir}/python2-path/python"
export PATH="${_builddir}/python2-path:${PATH}:${_builddir}/depot_tools"

# Get patches
git clone https://aur.archlinux.org/${mod_name}.git patches --depth 1 &

# Get depot_tools
git clone https://chromium.googlesource.com/chromium/tools/depot_tools depot_tools --depth 1 &

# Get electron9
git clone --branch=v${version} https://github.com/electron/electron.git ${mod_name} --depth 1 &

# Get chromium
git clone --branch=${_chromiumver} --depth=1 https://chromium.googlesource.com/chromium/src.git src &

wait

echo "git clone done"

echo "solutions = [
  {
    \"name\": \"src/${mod_name}\",
    \"url\": \"file://${_builddir}/${mod_name}\",
    \"deps_file\": \"DEPS\",
    \"managed\": False,
    \"custom_deps\": {
      \"src\": None,
    },
    \"custom_vars\": {},
  },
]" > .gclient

python3 "${_builddir}/depot_tools/gclient.py" sync \
    --with_branch_heads \
    --with_tags \
    --nohooks

sed -e "s/'am'/'apply'/" -i src/${mod_name}/script/lib/git.py

echo "Running hooks..."
python2 src/build/landmines.py
python2 src/build/util/lastchange.py -o src/build/util/LASTCHANGE
python2 src/build/util/lastchange.py -m GPU_LISTS_VERSION --revision-id-only --header src/gpu/config/gpu_lists_version.h
python2 src/build/util/lastchange.py -m SKIA_COMMIT_HASH -s src/third_party/skia --header src/skia/ext/skia_commit_hash.h
# Create sysmlink to system clang-format
ln -s /usr/bin/clang-format src/buildtools/linux64
# Create sysmlink to system Node.js
mkdir -p src/third_party/node/linux/node-linux-x64/bin
ln -sf /usr/bin/node src/third_party/node/linux/node-linux-x64/bin
python2 src/third_party/depot_tools/download_from_google_storage.py \
    --no_resume --extract --no_auth --bucket chromium-nodejs \
    -s src/third_party/node/node_modules.tar.gz.sha1
python2 src/tools/download_cros_provided_profile.py \
    --newest_state=src/chrome/android/profiles/newest.txt \
    --local_state=src/chrome/android/profiles/local.txt \
    --output_name=src/chrome/android/profiles/afdo.prof \
    --gs_url_base=chromeos-prebuilt/afdo-job/llvm
python2 src/${mod_name}/script/apply_all_patches.py \
    src/${mod_name}/patches/config.json
cd src/${mod_name}
yarn install --frozen-lockfile
cd ..

# Applying local patches...
patch -Np1 -d third_party/breakpad/breakpad < ${_builddir}/patches/breakpad-fix-for-non-constant-SIGSTKSZ.patch
patch -Np1 -i ${_builddir}/patches/ffmpeg5.patch
patch -Np1 -i ${_builddir}/patches/add-missing-algorithm-header-in-crx_install_error.cc.patch
patch -Np1 -i ${_builddir}/patches/avoid-double-destruction-of-ServiceWorkerObjectHost.patch
patch -Np1 -i ${_builddir}/patches/chromium-83-gcc-10.patch
patch -Np1 -i ${_builddir}/patches/chromium-ffmpeg-4.3.patch
patch -Np1 -i ${_builddir}/patches/chromium-include-limits.patch
patch -Np0 -i ${_builddir}/patches/chromium-skia-harmony.patch
patch -Np1 -i ${_builddir}/patches/sandbox-build-if-glibc-2.34-dynamic-stack-size-is-en.patch
patch -Np1 -i ${_builddir}/patches/sql-make-VirtualCursor-standard-layout-type.patch
patch -Np1 -F3 -i ${_builddir}/patches/std-max-fix.patch
patch -Np0 -i ${_builddir}/patches/disk_data_allocator-Metadata-constructor.patch
patch -Np4 -i ${_builddir}/patches/qt5-webengine-glibc-2.33.patch
patch -Np1 -i ${_builddir}/patches/chromium-harfbuzz-3.0.0.patch
patch -Np1 -d third_party/skia < ${_builddir}/patches/skia-harfbuzz-3.0.0.patch
patch -Np1 -i ${_builddir}/patches/clean-up-a-call-to-set_utf8.patch
patch -Np1 -i ${_builddir}/patches/include-memory-header-to-get-the-definition-of-std-u.patch
patch -Np1 -F3 -i ${_builddir}/patches/iwyu-std-numeric_limits-is-defined-in-limits.patch
patch -Np1 -i ${_builddir}/patches/libstdc-fix-incomplete-type-in-AXTree-for-NodeSetSiz.patch
patch -Np1 -i ${_builddir}/patches/make-some-of-blink-custom-iterators-STL-compatible.patch
patch -Np1 -d v8 < ${_builddir}/patches/v8-call-new-ListFormatter-createInstance.patch
patch -Np1 -d v8 < ${_builddir}/patches/v8-remove-soon-to-be-removed-getAllFieldPositions.patch
patch -Np1 -i ${_builddir}/patches/use-system-libraries-in-node.patch
patch -Np1 -i ${_builddir}/patches/default_app-icon.patch

echo "Patching Chromium for using system libraries..."
_system_libs=('ffmpeg'
    'flac'
    'fontconfig'
    'freetype'
    'harfbuzz-ng'
    'icu'
    'libdrm'
    'libevent'
    'libjpeg'
    'libvpx'
    'libwebp'
    'libxml'
    'libxslt'
    'opus'
    're2'
    'snappy'
    'yasm'
    'zlib'
)

sed -i 's/OFFICIAL_BUILD/GOOGLE_CHROME_BUILD/' \
    tools/generate_shim_headers/generate_shim_headers.py
for lib in $(printf "%s\n" "${_system_libs[@]}" | sed 's/^libjpeg$/&_turbo/'); do
    third_party_dir="third_party/${lib}"
    if [ ! -d ${third_party_dir} ]; then
    third_party_dir="base/${third_party_dir}"
    fi
    find ${third_party_dir} -type f \
        \! -path "${third_party_dir}/chromium/*" \
        \! -path "${third_party_dir}/google/*" \
        \! -path 'third_party/harfbuzz-ng/utils/hb_scoped.h' \
        \! -path 'third_party/yasm/run_yasm.py' \
        \! -regex '.*\.\(gn\|gni\|isolate\)' \
        -delete
done
python2 build/linux/unbundle/replace_gn_files.py \
    --system-libraries \
    "${_system_libs[@]}"
